Scríobaire
==========

<blockquote><dfn><a href="javascript:document.querySelector('#pronounce').play()">ʃkɾibɛɾə</a></dfn></blockquote>

A HTML scraper library in Go. Deliberately designed to have a similar
unmarshalling interface to [encoding/json][json] or [encoding/xml][xml], it'll
use [xpath][] or [CSS selectors][css] defined in struct comment fields to
extract types from a stream of HTML into a target.

You can read more in the [godocs][].

Examples
--------

```!go
// Package main is an example of how to use the library.
package main

import "github.com/bjjb/html"

func main() {
  type Item struct {
    ID   uint `xpath:"//li/@id"`
    Text uint `xpath:"//li/text()"`
  }

  type List struct {
    Items []*Item `css:"ul"`
  }

  stream := strings.NewReader(`
    <ul>
      <li id='1'>A</li>
      <li id='2'>B</li>
      <li id='3'>C</li>
    </ul>
  `)

  list := &List{}
  if err := html.Decoder(stream).Decode(list); err != nil {
    log.Fatal(err)
  }

  for _, item := list.Items {
    fmt.Printf("Item %d = %q\n", item.ID, item.Text)
  }
}
```

<audio id='pronounce' src='data:audio/opus;base64,T2dnUwACAAAAAAAAAAAKivj7AAAAAC3fPXgBE09wdXNIZWFkAQE4AYC7AAAAAABPZ2dTAAAAAAAAAAAAAAqK+PsBAAAAbqPv1QE/T3B1c1RhZ3MNAAAATGF2ZjU3LjgzLjEwMAEAAAAeAAAAZW5jb2Rlcj1MYXZjNTcuMTA3LjEwMCBsaWJvcHVzT2dnUwAAgLsAAAAAAAAKivj7AgAAALRV6nAyBwoLDA0MDQwQDAwLCgsLFRQVFhUUFhUQEhAWFRQXExIXFBQVDxgXGhcTFhcWGhcXFRYIC+RTFUvyCAgabAUe18jFIAgJuvcxTiN0q1pkCAp4w4ruKCHiEgoECAp83Ol4aYvqQ+pZIAgKeMONAme/7SBHgAgKfNzlfwXz0eQsxIAICoMEUmswyfxwDOAIC6773qPTYKJVOJAWxCjsCAwmDBM0u9uBWYrPCAtc++RUoTTXAF7QCAs9Mlgyd681/P0ICn07QeuJh4AzCAp4slDL7hGbCdgICni1HqAT+UnZgAiGGxwJJjhdis0bkyOxepc4X2mhcAiJWB7ycYmKGcFrpwvBnhcxQllMCI4poGPKA61zufhCSC/5V48JFNeACI/SNEZ2ctY3Iztup9Vjm8H7ZygdQAiQ6t8VQq5s5FTVEnXKsOK/RXpOgAiSMclY1M1x33HJXrROIPMxUYlmCJNS8g5uOXhSGi7+r8TZpAh78FkouQiUn2SJCpfMWOkOcMmiCopV0PZfaQiD9esyujb+dkToBv8V22AIjEBC6VJ/853qvq9DwhIwB7gIgJlU4k41HQrvSRpD346qCIsN7nhPda6/76W/bkrOv7Ec1cpisAiEVzzNQpDbCIaR5Ld/Uk8q2Gk7wAiFkyKWk0GXWZRe0fOSE+5JFs7wCIUevtGEE4jGM9rpLxDsC1rQ8sR+iEAIuUoqUMDk1bw51XFu45qpeg2SCLTH9i5ZA98GmtFDnRVAKXawCLZNPgW2XsiRcGGsQR9HcNr5bbwijPcIt9SpHepXcFWwFdgK9XL3tS8lBAi20ZjZ7XZLQMA+usj4sLyFptGACLW+0xNuB+3mh2VaFxaFDNxgo8qACLUi0vDZnAr+jpfw9K4YCLVs1j36RdXttSXKa9dOoe4yh2eH0vWgCLcJzof/Qgj6kyEwFSGtelJttyDUP0AIt5Abl79d4hVmMWxsI2eD8n7T8MpA9+H4YAi4TD2fu4f9JYo2goai4M/rDY/3/97wCIK8GF8SrZCzliq+Ch7iCRVcAQicaPEXyx+wHI9z/Y/nRHCH9EVUy2AIm1O0nXUnBlpOtGhuek9UZuhtASrIwAifOUbLRlaON9zTH1E6UVn7c/A6L+AIsq9R73iQ3ZaerNtrlYBnnnQ9sP1qw1MLIAi2TGkNpbsOU++QoNA5QkJkXCbf+2HgCLpRlhDTZZ0SWhvtgFT9P0yx2whI2xwIk93GUwl9sBnIylOV9xVmItKNoVYIhJlGt8x2soka09HsGMbykJwRg2iqT2dnUwAEuEkBAAAAAAAKivj7AwAAAIeJXJsmFBQZFRkYGBMQEhAUEhAMDw8KDgsLDA0MCwsKCQkMCgkJCwkHCAYIhQQH82RLivaaVaIJ28v+JSaG7gi4DSzTm+KueNpVawIdrexYiy3QCLcTBcJQR0smqCEUmG2IwB5kKcoNqVr3wAi5hoLuCLmhDEr5m9m5e5JKyZnptwi4yOH3gPPXix+BnD7hHnv3s9TcheBwrHAIujFi6AUNIfnuleybLRRmnNyy0pGw+loIs6mZRKMjPIQeVhC9QPKd2bb/YSKdgcAIko/yR708IBX6ub2n1doqyugQCJFjelnS51sFrWnYpALy7wiPKZQ9BqB7fOuG6noETGupgAiNY346q9Lxpdz3OCX7pwQIN9H2s7gomV0cPDvhRBHOeWA/wAgutsugk3EtTzPSm0TTFdYC0Agq5ZbGkgLnwZJ1RUkIHoAIA11O2OQrYn2naEAIHb/uwE06e6hNVj3HZkQIF4IEn450enk1TrrZ+KAIDpdv9nJ0U6iVCA05GGDXFsIFWy8voIAIDOHGv4so97YrHQgLV+JeF6N44vmECAsbsQbQbgr+IUNsCAqDLNYH/L21FkOo4AgKeMOPb81+Mesp8AgKfMd5C4h0g0qQCAp4Oj9Ud9C/FqAICnhMm449Wr+ACAqDJtUTtX0YCAp3GCEaLYdECAp3Ff4genw1/5ywCAp4Tf70yi9hUAgKdxW9xCeKXAgKeLFqf6AbXggKaChNi31U7EpQCAmSsWnyzTtgCAlxdU9VYggIgsYe9iS4CAbgsw7G'></audio>

[play]: javascript:document.querySelector('#pronounce').play()
[json]: https://godoc.org/encoding/json
[xml]: https://godoc.org/encoding/xml
[xpath]: https://www.w3.org/TR/xpath
[css]: https://www.w3.org/TR/selectors-3/
[godocs]: https://godoc.org/github.com/bjjb/html
