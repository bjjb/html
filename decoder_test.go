package html

import (
	"io"
	"reflect"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestDecode(t *testing.T) {
	stream := strings.NewReader("<span id='1'>Hello</span>")
	target := &struct{}{}
	assert.NoError(t, NewDecoder(stream).Decode(target))
}

type Decoder struct {
	r   io.Reader
	err error
}

type UnmarshalError struct {
	Type reflect.Type
}

func (e *UnmarshalError) Error() string {
	if e.Type == nil {
		return "html: Unmarshal(nil)"
	}
	if e.Type.Kind() != reflect.Ptr {
		return "html: Unmarshal(non-pointer " + e.Type.String() + ")"
	}
	return "html: Unmarshal(nil " + e.Type.String() + ")"
}

func (dec *Decoder) Decode(v interface{}) error {
	rv := reflect.ValueOf(v)
	if rv.IsNil() || rv.Kind() != reflect.Ptr {
		return &UnmarshalError{reflect.TypeOf(v)}
	}
	return dec.decode(v)
}

func (dec *Decoder) decode(v interface{}) error {
	return nil
}

func NewDecoder(r io.Reader) *Decoder {
	return &Decoder{r: r}
}
